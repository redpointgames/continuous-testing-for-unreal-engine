// Copyright June Rhodes. MIT Licensed.

#include "UnrealEngineGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UnrealEngineGame, "UnrealEngineGame" );
