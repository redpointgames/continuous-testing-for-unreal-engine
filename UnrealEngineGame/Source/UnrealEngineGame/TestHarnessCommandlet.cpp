// Copyright June Rhodes. MIT Licensed.

#include "TestHarnessCommandlet.h"

DEFINE_LOG_CATEGORY_STATIC(LogTestHarness, All, All);

#define END_TEST(Value, Reason)                                                                                        \
    {                                                                                                                  \
        bTestHarnessSuccess = Value;                                                                        \
        RequestEngineExit(FString::Printf(TEXT("Test harness complete; exit reason: %s"), Reason));                \
    }

bool bTestHarnessSuccess = false;

UTestHarnessCommandlet::UTestHarnessCommandlet(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
    // These are REQUIRED in order for the commandlet to work.
    IsClient = false;
    IsServer = false;
    IsEditor = false;

    LogToConsole = true;
    ShowErrorCount = true;
    ShowProgress = true;
}


void UTestHarnessCommandlet::ProgressExample()
{
    // This shows you how you can emit progress to the console. The test harness script
    // that runs inside the container looks for TestHarness= and parses the JSON out. You can 
    // emit whatever data you want, you just need to update the test harness script to
    // then pass it to the monitoring server.

    TSharedRef<FJsonObject> Json = MakeShared<FJsonObject>();
    Json->SetStringField(TEXT("String1"), TEXT("Whatever"));
    Json->SetStringField(TEXT("String2"), TEXT("You"));
    Json->SetStringField(TEXT("String3"), TEXT("Want"));

    FString OutputString;
    TSharedRef<TJsonWriter<>> Writer = TJsonWriterFactory<>::Create(&OutputString);
    FJsonSerializer::Serialize(Json, Writer);
    OutputString.ReplaceInline(TEXT("\n"), TEXT(""));

    UE_LOG(LogTestHarness, Verbose, TEXT("TestProgress=%s"), *OutputString);
}

void UTestHarnessCommandlet::ResultExample()
{
    // This shows you how you can emit a result to the console. Same with progress,
    // the test harness script inside the container defines how this data is then
    // sent over to the monitoring server.

    TArray<TSharedPtr<FJsonValue>> ArrayExample;

    TSharedRef<FJsonObject> Json = MakeShared<FJsonObject>();
    Json->SetStringField(TEXT("StringVal"), TEXT("Again"));
    Json->SetArrayField(TEXT("ArrayExample"), ArrayExample);

    FString OutputString;
    TSharedRef<TJsonWriter<>> Writer = TJsonWriterFactory<>::Create(&OutputString);
    FJsonSerializer::Serialize(Json, Writer);
    OutputString.ReplaceInline(TEXT("\n"), TEXT(""));

    UE_LOG(LogTestHarness, Verbose, TEXT("TestResult=%s"), *OutputString);

    // End the test, if appropriate.
    END_TEST(true, TEXT("Test harness complete"));
}

int32 UTestHarnessCommandlet::Main(const FString& Params)
{
    // Kick off some asynchronous work here. For the matchmaking plugin, we use the online
    // subsystem API to start some asynchronous operations.

    // Then run the main game loop. This allows asynchronous operations to proceed.
    if (!IsEngineExitRequested())
    {
        GIsRunning = true;
        while (GIsRunning && !IsEngineExitRequested())
        {
            GEngine->UpdateTimeAndHandleMaxTickRate();
            GEngine->Tick(FApp::GetDeltaTime(), false);
            GFrameCounter++;
            FTicker::GetCoreTicker().Tick(FApp::GetDeltaTime());
            FPlatformProcess::Sleep(0);
        }
        GIsRunning = false;
    }

    if (bTestHarnessSuccess)
    {
        UE_LOG(LogTestHarness, Verbose, TEXT("Test harness succeeded!"));
    }
    else
    {
        UE_LOG(LogTestHarness, Error, TEXT("Test harness failed!"));
    }
    return bTestHarnessSuccess ? 0 : 1;
}
