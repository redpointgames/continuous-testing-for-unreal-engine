// Copyright June Rhodes. MIT Licensed.

using UnrealBuildTool;

public class UnrealEngineGame : ModuleRules
{
	public UnrealEngineGame(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });

		PrivateDependencyModuleNames.AddRange(new string[] { "Json" });
	}
}
