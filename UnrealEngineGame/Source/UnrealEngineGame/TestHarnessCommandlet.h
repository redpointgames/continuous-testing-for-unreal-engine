// Copyright June Rhodes. MIT Licensed.

#pragma once

#include "CoreMinimal.h"
#include "Commandlets/Commandlet.h"

#include "TestHarnessCommandlet.generated.h"

UCLASS()
class UNREALENGINEGAME_API UTestHarnessCommandlet : public UCommandlet
{
	GENERATED_UCLASS_BODY()

    void ProgressExample();

    void ResultExample();

public:
	virtual int32 Main(const FString& Params) override;
};
