param()

$global:Execute = $true

trap {
  Write-Output "Got trap: $_"
  $global:Execute = $false
  kubectl delete pod --wait=false --now=true -l "role=test-harness"
  exit 1
}

$TagToUse = ((kubectl get pod matchmaking-stream-manager -o json | ConvertFrom-Json).spec.containers[0].image).Split("@")[0].Split(":")[1]
Write-Host "Using tag: $TagToUse"

$global:AccumulatedDelay = 0
$global:PodIndex = 0

while ($global:Execute) {
  $LowerBoundSeconds = 2
  $UpperBoundSeconds = 5
  $Delay = ((((([System.Math]::Cos(((Get-Date).Minute / 10) * [System.Math]::Pi * 2) + 1) / 2) * ($UpperBoundSeconds - $LowerBoundSeconds)) + $LowerBoundSeconds) * 1000)
  Write-Host "Running with delay $Delay ms"

  $StartTimestamp = (Get-Date)

  $KubectlOutput = (kubectl get pods -l "role=test-harness" --field-selector status.phase=Pending 2>&1 | Out-String)
  Write-Host $KubectlOutput
  if (!$KubectlOutput.Trim().Contains("No resources found")) {
    Start-Sleep -Milliseconds $Delay
    continue
  }

  $PodsToCreate = 1
  while ($global:AccumulatedDelay -gt $Delay) {
    $PodsToCreate++
    $global:AccumulatedDelay -= $Delay
  }

  # Don't make too many, otherwise the cluster will get overloaded...
  if ($PodsToCreate -gt 8) {
    $PodsToCreate = 8
  }

  ##
  ## NOTE: You'll want to adapt the logic below to your particular use case.
  ##

  $PodYaml = ""
  for ($i = 0; $i -lt $PodsToCreate; $i++) {

    $PartySize = 1
    if (($global:PodIndex % 4) -eq 0) {
      # Make sure we alternate between parties and solo to ensure
      # we have enough players to fill out the matches.
      $PartySize = (Get-Random -Minimum 1 -Maximum 5)
    }
    $global:PodIndex += 1

    $PodId = "$((Get-Date).Ticks)-$i-size-$PartySize"
    $PartyId = "Party$PodId"
    $IsSplitParty = ((Get-Random -Minimum 1 -Maximum 3) -eq 1)

    if ($IsSplitParty) {
      # We make a pod for each party member, and they run in their own process
      for ($p = 0; $p -lt $PartySize; $p++) {
        $StreamJobId = "Streaming$PodId-$p"
        $RequestsCpu = 750 * 1;
        $RequestsMem = 1024 * 1;
        $LimitsCpu = 2000 * 1;
        $LimitsMemory = 1024 * 1;
        $PodYaml += @"
apiVersion: v1
kind: Pod
metadata:
  name: harness-$PodId-idx-$p
  labels:
    role: "test-harness"
    instance: "$PodId"
spec:
  serviceAccountName: test-harness
  restartPolicy: Never
  imagePullSecrets:
    - name: gitlab
  containers:
    - name: matchmaking-test-harness
      image: $($env:IMAGE_PATH):$TagToUse
      resources:
        requests:
          cpu: ${RequestsCpu}m
          ephemeral-storage: 1Gi
          memory: ${RequestsMem}Mi
        limits:
          cpu: ${LimitsCpu}m
          ephemeral-storage: 1Gi
          memory: ${LimitsMemory}Mi
      env:
        - name: RUN_ID
          value: "streaming"
        - name: QUEUE_NAME
          value: "MMTestHarnessStreaming"
        - name: PARTY_ID
          value: "$PartyId"
        - name: PARTY_INDEX
          value: "$p"
        - name: PARTY_SIZE
          value: "$PartySize"
        - name: PARTY_IS_SPLIT
          value: "true"
        - name: POD_PREFIX
          value: "harness-$PodId-idx-"
        - name: JOB_ID
          value: "$StreamJobId"
        - name: JOB_TOTAL
          value: "__unused__"
        - name: MONITORING_SERVER_ENDPOINT
          value: "$env:MONITORING_SERVER_ENDPOINT"
---

"@
      }
    }
    else {
      # We make a pod for all the party members as if they're all playing split screen, in a single process
      $StreamJobId = "Streaming$PodId"
      $RequestsCpu = 750 * $PartySize;
      $RequestsMem = 1024 * $PartySize;
      $LimitsCpu = 2000 * $PartySize;
      $LimitsMemory = 1024 * $PartySize;
      $PodYaml += @"
apiVersion: v1
kind: Pod
metadata:
  name: harness-$PodId
  labels:
    role: "test-harness"
    instance: "$PodId"
spec:
  serviceAccountName: test-harness
  restartPolicy: Never
  imagePullSecrets:
    - name: gitlab
  containers:
    - name: matchmaking-test-harness
      image: $($env:IMAGE_PATH):$TagToUse
      resources:
        requests:
          cpu: ${RequestsCpu}m
          ephemeral-storage: 1Gi
          memory: ${RequestsMem}Mi
        limits:
          cpu: ${LimitsCpu}m
          ephemeral-storage: 1Gi
          memory: ${LimitsMemory}Mi
      env:
        - name: RUN_ID
          value: "streaming"
        - name: QUEUE_NAME
          value: "MMTestHarnessStreaming"
        - name: PARTY_ID
          value: "$PartyId"
        - name: PARTY_SIZE
          value: "$PartySize"
        - name: PARTY_IS_SPLIT
          value: "false"
        - name: JOB_ID
          value: "$StreamJobId"
        - name: JOB_TOTAL
          value: "__unused__"
        - name: MONITORING_SERVER_ENDPOINT
          value: "$env:MONITORING_SERVER_ENDPOINT"
---

"@
    }
  }
  Write-Host "Creating $($PodsToCreate+1) new streaming jobs..."
  $PodYaml | kubectl apply -f -

  # Delete successful pods that are older than 15 minutes
  kubectl get pods --field-selector status.phase=Succeeded -o go-template --template "{{range .items}}{{.metadata.name}} {{.metadata.creationTimestamp}}`n{{end}}" -l "role=test-harness" | Select-Object @{ Name="PodName"; Expression = { $_.Split(" ")[0] }; },@{ Name="DateCreated"; Expression = { [DateTime]::ParseExact($_.Split(" ")[1], "yyyy-MM-ddTHH:mm:ssZ", $null).ToUniversalTime() }; } | Where-Object -Property DateCreated -LT -Value ([DateTime]::UtcNow.AddMinutes(-15)) | ForEach-Object { kubectl delete pod --wait=false --now=true $_.PodName }

  # Delete pods that are stuck after 15 minutes
  kubectl get pods --field-selector status.phase=Running -o go-template --template "{{range .items}}{{.metadata.name}} {{.metadata.creationTimestamp}}`n{{end}}" -l "role=test-harness" | Select-Object @{ Name="PodName"; Expression = { $_.Split(" ")[0] }; },@{ Name="DateCreated"; Expression = { [DateTime]::ParseExact($_.Split(" ")[1], "yyyy-MM-ddTHH:mm:ssZ", $null).ToUniversalTime() }; } | Where-Object -Property DateCreated -LT -Value ([DateTime]::UtcNow.AddMinutes(-15)) | ForEach-Object { kubectl delete pod --wait=false --now=true $_.PodName }

  $EndTimestamp = (Get-Date)

  $ElapsedMilliSecondsForDeploy = ($EndTimestamp - $StartTimestamp).TotalMilliseconds
  $ActualDelayToApply = $Delay - $ElapsedMilliSecondsForDeploy
  if ($ActualDelayToApply -gt 0) {
    Start-Sleep -Milliseconds $Delay
  }
  else {
    $global:AccumulatedDelay += - $ActualDelayToApply
  }
}

exit 1