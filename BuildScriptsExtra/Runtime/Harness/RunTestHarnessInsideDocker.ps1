param()

trap {
    Write-Error $_
    exit 1
}

if ($env:RUN_ID -eq $null) {
    Write-Error "RUN_ID environment variable not set."
    exit 1
}
if ($env:JOB_ID -eq $null) {
    Write-Error "JOB_ID environment variable not set."
    exit 1
}
if ($env:MONITORING_SERVER_ENDPOINT -eq $null) {
    Write-Error "MONITORING_SERVER_ENDPOINT environment variable not set."
    exit 1
}

#
# An example on how to wait for related pods to also be scheduled in Kubernetes...
#
if ($env:PARTY_IS_SPLIT -eq "true") {
    while ($true) {
        $HasAnyPendingSiblings = $false
        for ($i = 0; $i -lt [int]::Parse($env:PARTY_SIZE); $i += 1) {
            if ($i -eq [int]::Parse($env:PARTY_INDEX)) {
                # No need to wait for ourselves.
                continue;
            }

            $Status = (kubectl get pods "$env:POD_PREFIX$i" "--output=jsonpath={.status.phase}" | Out-String).Trim()
            if ($Status -eq "Failed") {
                Write-Host "Required sibling pod has failed, also exiting with failure code!"
                exit 1
            }

            if ($Status -ne "Running") {
                Write-Host "Still waiting for $env:POD_PREFIX$i to start..."
                $HasAnyPendingSiblings = $true
                break;
            }
        }
        if ($HasAnyPendingSiblings) {
            Start-Sleep -Seconds 1
        } else {
            break
        }
    }
}

Add-Type -Path $PSScriptRoot\lib\Client.dll
$Rpc = New-Object Client.Client "$env:MONITORING_SERVER_ENDPOINT"

try {
    Write-Host "Connecting to monitoring server..."
    $Rpc.Ping((New-Object Monitoring.PingRequest @{
        RunId = $env:RUN_ID;
        JobId = $env:JOB_ID;
    }))
    Write-Host "Connected to monitoring server."

    $global:RegexAll = [regex]"\[(.*?)\]\[.*?\]([A-Za-z]+):\s([A-Za-z]+):\sTask\s([A-Za-z0-9]+):\s([A-Za-z]+):\s(.*)"
    $global:RegexTask = [regex]"\[(.*?)\]\[.*?\]([A-Za-z]+):\s([A-Za-z]+):\sTask\s([A-Za-z0-9]+):\s(.*)"
    $global:RegexGeneral = [regex]"\[(.*?)\]\[.*?\]([A-Za-z]+):\s([A-Za-z]+):\s(.*)"
    $global:RegexNoLevel = [regex]"\[(.*?)\]\[.*?\]([A-Za-z]+):\s(.*)"

    $global:DeterminedResults = @()
    $global:DidTestPass = $false
    $global:HasCriticalError = $false
    $global:CriticalErrorLine = ""
    $global:NoEmptyBuffer = $false
    $global:DidEmitStart = $false
    $global:ReceivedFirstLine = $false
    $global:TaskIds = @()
    $global:RecentLines = New-Object System.Collections.Generic.List[string]
    $global:LastLine = ""

    Write-Host "Invoking UnrealEngineGame.sh..."
    /app/UnrealEngineGame.sh -RUN=TestHarnessCommandlet -NullRHI -stdout -FullStdOutLogOutput | % {

        <#
        
        Most of the logic in this section will be specific to your test harness's behaviour and what
        data you emit from the commandlet.
        
        #>

        if (!$global:ReceivedFirstLine) {
            Write-Host "Received first line in PowerShell."
            $global:ReceivedFirstLine = $true
        }
        if ($_ -eq $null) {
            return
        }
        $CurrentLineForCompare = $_.Trim()
        if ($CurrentLineForCompare.Length -gt 30) {
            # Take off timestamp for double emit workaround.
            $CurrentLineForCompare = $CurrentLineForCompare.Substring(30)
        }
        if ($CurrentLineForCompare -eq $global:LastLine) {
        } else {
            Write-Host $_.Trim()
            $global:LastLine = $CurrentLineForCompare
            $LogTimestamp = ""
            $StepName = ""
            $TaskId = ""
            $SeverityLevel = ""
            $LogCategory = ""
            $LogMessage = ""
            if ($global:RegexAll.IsMatch($_.Trim())) {
                $Match = $global:RegexAll.Match($_.Trim());
                $LogTimestamp = $Match.Groups[1].Value
                $LogCategory = $Match.Groups[2].Value
                $SeverityLevel = $Match.Groups[3].Value
                $TaskId = $Match.Groups[4].Value
                $StepName = $Match.Groups[5].Value
                $LogMessage = $Match.Groups[6].Value
            } elseif ($global:RegexTask.IsMatch($_.Trim())) {
                $Match = $global:RegexTask.Match($_.Trim());
                $LogTimestamp = $Match.Groups[1].Value
                $LogCategory = $Match.Groups[2].Value
                $SeverityLevel = $Match.Groups[3].Value
                $TaskId = $Match.Groups[4].Value
                $StepName = ""
                $LogMessage = $Match.Groups[5].Value
            } elseif ($global:RegexGeneral.IsMatch($_.Trim())) {
                $Match = $global:RegexGeneral.Match($_.Trim());
                $LogTimestamp = $Match.Groups[1].Value
                $LogCategory = $Match.Groups[2].Value
                $SeverityLevel = $Match.Groups[3].Value
                $TaskId = ""
                $StepName = ""
                $LogMessage = $Match.Groups[4].Value
            } elseif ($global:RegexNoLevel.IsMatch($_.Trim())) {
                $Match = $global:RegexNoLevel.Match($_.Trim());
                $LogTimestamp = $Match.Groups[1].Value
                $LogCategory = $Match.Groups[2].Value
                $SeverityLevel = ""
                $TaskId = ""
                $StepName = ""
                $LogMessage = $Match.Groups[3].Value
            } else {
                $LogTimestamp = ""
                $LogCategory = ""
                $SeverityLevel = ""
                $TaskId = ""
                $StepName = ""
                $LogMessage = $_.Trim()
            }
            $Rpc.Log((New-Object Monitoring.LogRequest @{
                RunId = $env:RUN_ID;
                JobId = $env:JOB_ID;
                TaskId = $TaskId;
                Category = $LogCategory;
                Severity = $SeverityLevel;
                StepName = $StepName;
                Message = $LogMessage;
                Timestamp = $LogTimestamp;
            }))
        }
        if (!$global:HasCriticalError -and !$global:DidTestPass) {
            if ($global:RecentLines.Count -gt 20 -and !$global:NoEmptyBuffer) {
                $global:RecentLines.RemoveAt(0)
            }
            $global:RecentLines.Add($_.Trim())
        }
        if ($_.Contains("Assertion failed")) {
            $global:NoEmptyBuffer = $true
        }
        if ($_.Contains("Test harness succeeded!")) {
            $global:DidTestPass = $true
        }
        if ($_.Contains("CreateMatchmakingFollowRequestLobby: Creating a matchmaking follow request lobby") -and !$global:DidEmitStart) {
            <#
            Insert-Json -Json @{
                message_type = "job_starts";
                run_id = $env:RUN_ID;
                job_id = $env:JOB_ID;
                timestamp = "$(Get-Date -UFormat "%Y-%m-%dT%TZ")";
            }
            #>
            $global:DidEmitStart = $true
        }
        if (!$global:HasCriticalError) {
            if ($_.Contains("Critical error while matchmaking") -or $_.Contains("Test harness failed!")) {
                $global:HasCriticalError = $true
                $global:DidTestPass = $false
                $global:CriticalErrorLine = $_.Trim()
            }
        }
        if ($_.Contains("Engine crash handling finished")) {
            $global:HasCriticalError = $true
        }
        if ($_.Contains("LogTestHarness: Verbose: TestResult=")) {
            $ResultStr = $_.Substring($_.IndexOf("LogTestHarness: Verbose: TestResult=") + "LogTestHarness: Verbose: TestResult=".Length)
            $ResultStr = $ResultStr.Trim()
            $ResultJson = $ResultStr | ConvertFrom-Json

            $Request = (New-Object Monitoring.TaskResultsRequest @{
                RunId = $env:RUN_ID;
                JobId = $env:JOB_ID;
                TaskId = $ResultJson.TaskId;
                IsSuccess = $true;
                IsFilled = ($FilledSlots -eq $TotalSlots -and $TotalSlots -gt 0);
                RecentLines = ($global:RecentLines -join "`n");
            })

            $TotalSlots = 0
            $FilledSlots = 0
            foreach ($Team in $ResultJson.Teams) {
                $TeamInfo = New-Object Monitoring.TeamInfo
                foreach ($Slot in $Team.Slots) {
                    $TotalSlots += 1
                    if ($Slot.Type -eq "User") {
                        $FilledSlots += 1
                    }
                    $TeamInfo.Slots.Add((New-Object Monitoring.SlotInfo @{
                        Type = $Slot.Type;
                        UserId = $Slot.UserId;
                    }));
                }
                $Request.Teams.Add($TeamInfo);
            }
            $Request.MatchFillRate = 0;
            if ($TotalSlots -gt 0) {
                $Request.MatchFillRate = ($FilledSlots / $TotalSlots);
            }

            $global:DeterminedResults += $Request
        }
        if ($_.Contains("LogTestHarness: Verbose: TestProgress=")) {
            $ResultStr = $_.Substring($_.IndexOf("LogTestHarness: Verbose: TestProgress=") + "LogTestHarness: Verbose: TestProgress=".Length)
            $ResultStr = $ResultStr.Trim()
            $ResultJson = $ResultStr | ConvertFrom-Json

            $Rpc.TaskProgress((New-Object Monitoring.TaskProgressRequest @{
                RunId = $env:RUN_ID;
                JobId = $env:JOB_ID;
                TaskId = $ResultJson.TaskId;
                UserId = $ResultJson.UserId;
                PartyId = $ResultJson.PartyId;
                StepName = $ResultJson.StepName;
                CurrentStatus = $ResultJson.CurrentStatus;
                CurrentDetail = $ResultJson.CurrentDetail;
            }))
            if (!($global:TaskIds -contains $ResultJson.TaskId)) {
                $global:TaskIds += $ResultJson.TaskId
            }
        }
    }

    Write-Output "[Result processing]"
    $FoundTaskIds = @()
    foreach ($DeterminedResult in $global:DeterminedResults) {
        $FoundTaskIds += $DeterminedResult.TaskId
        $DeterminedResult.IsSuccess = $global:DidTestPass -and !$global:HasCriticalError;
        $DeterminedResult.CriticalErrorMessage = $global:CriticalErrorLine;
        Write-Output "[Result processing] Found results for task $($DeterminedResult.TaskId)"
    }
    foreach ($TaskId in $global:TaskIds) {
        if (!$FoundTaskIds.Contains($TaskId)) {
            $global:DeterminedResults += (New-Object Monitoring.TaskResultsRequest @{
                RunId = $env:RUN_ID;
                JobId = $env:JOB_ID;
                TaskId = $TaskId;
                IsSuccess = $false;
                IsFilled = $false;
                MatchFillRate = 0;
                Teams = @();
                RecentLines = ($global:RecentLines -join "`n");
            })
        } else {
            Write-Output "[Result processing] Substituted in results for $($DeterminedResult.TaskId), because it never returned results"
        }
    }
    foreach ($DR in $global:DeterminedResults) {
        Write-Output "[Result processing] Inserting results for task $($DR.TaskId)"
        $Rpc.TaskResults($DR)
    }
    if ($global:DidTestPass -and !$global:HasCriticalError) {
        exit 0
    }
    exit 1
} finally {
    Write-Host "Disposing of RPC client..."
    $Rpc.Dispose()
    Write-Host "Disposed RPC client."
}