## Build .NET module for loading into PowerShell
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS net-lib

# Install PowerShell
ENV PATH="/root/.dotnet/tools:${PATH}"
RUN dotnet tool install --global PowerShell --version 7.0.3

# Copy source code
COPY Monitoring/Client /build/Client
COPY Monitoring/Server/monitoring.proto /Monitoring/Server/monitoring.proto
WORKDIR /build/Client

# Build
RUN dotnet publish -r win-x64 -c Debug

## Runtime image that executes the Unreal Engine client
FROM ubuntu:20.04

# Install PowerShell, Kubernetes and Google Cloud SDK
RUN apt-get update && apt-get install -y apt-transport-https ca-certificates gnupg wget software-properties-common curl
RUN update-ca-certificates
RUN wget -q https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb
RUN dpkg -i packages-microsoft-prod.deb
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
RUN apt-get update && add-apt-repository universe && apt-get install -y powershell google-cloud-sdk kubectl

# Set up environment
USER 1000:1000
ENV HOME=/app

# Copy content
ADD --chown=1000:1000 UnrealEngineGame/Saved/StagedBuilds/LinuxNoEditor /app
ADD --chown=1000:1000 BuildScriptsExtra/Runtime/Harness/RunTestHarnessInsideDocker.ps1 /app
COPY --from=net-lib /build/Client/bin/Debug/net5.0/win-x64/publish /app/lib

# Fix up permissions and set entrypoint
RUN chmod a+x /app/UnrealEngineGame.sh /app/UnrealEngineGame/Binaries/Linux/UnrealEngineGame-Linux-DebugGame
RUN chmod a+rwX /app
WORKDIR /app
ENTRYPOINT [ "pwsh", "-File", "/app/RunTestHarnessInsideDocker.ps1" ]