# === Restore yarn ===
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS yarn-cache

# Configure package manager
RUN apt-get update && apt-get install -y apt-transport-https nodejs npm && npm install --global yarn && apt-get upgrade -y

# Copy just package.json and restore
COPY Monitoring/Server/ClientApp/package.json /restore/package.json
COPY Monitoring/Server/ClientApp/yarn.lock /restore/yarn.lock
WORKDIR /restore
ENV YARN_CACHE_FOLDER=/usr/local/yarn-cache
RUN yarn install

# === Build environment ===
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS builder

# Configure package manager
RUN apt-get update && apt-get install -y apt-transport-https nodejs npm && npm install --global yarn && apt-get upgrade -y

# Install Powershell
ENV PATH="/root/.dotnet/tools:${PATH}"
RUN dotnet tool install --global PowerShell --version 7.0.3

# Copy source code
COPY Monitoring/Client /build/Client
COPY Monitoring/Server /build/Server
WORKDIR /build/Server

# Copy restored artifacts
ENV YARN_CACHE_FOLDER=/usr/local/yarn-cache
COPY --from=yarn-cache /usr/local/yarn-cache /usr/local/yarn-cache
COPY --from=yarn-cache /restore/node_modules /build/Server/ClientApp/node_modules

# Build 
RUN dotnet restore -v minimal Server.sln
RUN dotnet publish -v normal -o /pkg -c Release Server.csproj

# Test
RUN dotnet test -v normal -c Release --no-restore Server.sln

# === Runtime image ===
FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS final

# Make /var/www directory for runtime
RUN mkdir /var/www && chown www-data -R /var/www && chmod -R a+rwX /var/www

# Copy build
RUN chown www-data -R /srv && chmod -R a+rwX /srv
WORKDIR /srv
COPY --from=builder --chown=www-data:www-data /pkg/ /srv/
USER www-data
EXPOSE 5000

# Environment defaults
ENV ASPNETCORE_URLS http://+:5000

# Configure how to launch
ENTRYPOINT ["dotnet", "Server.dll"]