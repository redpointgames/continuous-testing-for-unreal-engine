#!/bin/bash

set -e
set -x

skaffold run --kube-context="monitoring-context" -f BuildScriptsExtra/Monitoring/skaffold.yaml
kubectl rollout status -w deployments/monitoring -n your-deployment-namespace
