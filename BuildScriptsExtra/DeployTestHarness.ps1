param([string] $EnginePath, [string] $StageDirectory)

$KubectlContext="your-cluster-context"

trap {
    kubectl --context="$KubectlContext" delete pod --wait=false --now=true -l "role=streaming-manager"
    kubectl --context="$KubectlContext" delete pod --wait=false --now=true -l "role=test-harness"
}

Write-Host "Building the container images..."
Push-Location "$PSScriptRoot\.."
try {
  skaffold build --kube-context="$KubectlContext" -f "$PSScriptRoot/Runtime/skaffold.yaml" --file-output="$PSScriptRoot\artifacts.json"
  if ($LastExitCode -ne 0) {
    exit $LastExitCode
  }
} finally {
  Pop-Location
}

Write-Output "Deleting any existing test harness pods..."
kubectl --context="$KubectlContext" delete pod --wait=false --now=true -l "role=streaming-manager"

Write-Host "Deploying to $KubectlContext context..."
Push-Location "$PSScriptRoot\.."
try {
  skaffold deploy --kube-context="$KubectlContext" -f "$PSScriptRoot/Runtime/skaffold.yaml" --build-artifacts="$PSScriptRoot\artifacts.json"
  if ($LastExitCode -ne 0) {
    exit $LastExitCode
  }
} finally {
  Pop-Location
}

Write-Host "Streaming deployment complete. Open the monitoring or use kubectl get pods to see what's running."