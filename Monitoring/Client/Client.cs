﻿using Grpc.Net.Client;
using System;
using System.Net.Http;
using static Monitoring.MonitoringRpc;

namespace Client
{
    public class Client : IDisposable
    {
        private readonly MonitoringRpcClient _grpc;
        private GrpcChannel _channel;

        public Client(string endpoint)
        {
            AppContext.SetSwitch(
       "System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

            var httpHandler = new HttpClientHandler();
            httpHandler.ServerCertificateCustomValidationCallback =
                HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;
            _channel =
            GrpcChannel.ForAddress(endpoint,
    new GrpcChannelOptions { HttpHandler = httpHandler });
            _grpc = new MonitoringRpcClient(_channel);
        }

        public string Ping(Monitoring.PingRequest request)
        {
            return _grpc.Ping(request).ServerMessage;
        }

        public void Log(Monitoring.LogRequest request)
        {
            try
            {
                _grpc.Log(request);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("gRPC Error: " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void TaskProgress(Monitoring.TaskProgressRequest request)
        {
            try
            {
                _grpc.TaskProgress(request);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("gRPC Error: " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void TaskResults(Monitoring.TaskResultsRequest request)
        {
            try
            {
                _grpc.TaskResults(request);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("gRPC Error: " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            _channel.Dispose();
        }
    }
}
