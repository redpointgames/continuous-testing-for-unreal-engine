﻿namespace Server.Processors
{
    using Microsoft.AspNetCore.SignalR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using Monitoring;
    using Server.Database;
    using Server.Readers;
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    public class TaskProcessor : ITaskProcessor, IHostedService
    {
        private class TaskProcessorInfo
        {
            public TaskProgressRequest LatestStatus;
            public TaskResultsRequest Results;
        }

        private ConcurrentQueue<TaskProcessorInfo> _pendingTaskUpdates;
        private Task _backgroundTask;
        private bool _isRunning;
        private readonly ILogger<TaskProcessor> _logger;
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly IHubContext<MatchmakingDataHub> _hubContext;

        public TaskProcessor(ILogger<TaskProcessor> logger, IServiceScopeFactory scopeFactory, IHubContext<MatchmakingDataHub> hubContext)
        {
            _logger = logger;
            _scopeFactory = scopeFactory;
            _hubContext = hubContext;
            _pendingTaskUpdates = new ConcurrentQueue<TaskProcessorInfo>();
        }

        public void EnqueueProgress(TaskProgressRequest request)
        {
            var taskInfo = new TaskProcessorInfo { LatestStatus = request };
            _pendingTaskUpdates.Enqueue(taskInfo);
        }

        public void EnqueueResults(TaskResultsRequest request)
        {
            var taskInfo = new TaskProcessorInfo { Results = request };
            _pendingTaskUpdates.Enqueue(taskInfo);
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _backgroundTask = Task.Run(ProcessTasks);
            _isRunning = true;
            return Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _isRunning = false;
            await _backgroundTask;
        }

        private async Task ProcessTasks()
        {
            var batch = new List<TaskProcessorInfo>();
            while (_isRunning)
            {
                TaskProcessorInfo nextUpdate;
                if (_pendingTaskUpdates.TryDequeue(out nextUpdate))
                {
                    batch.Add(nextUpdate);
                    continue;
                }

                if (batch.Count == 0)
                {
                    await Task.Yield();
                    continue;
                }

                var stopwatch = Stopwatch.StartNew();

                try
                {
                    // Now we have a batch of task updates to apply.
                    using (var scope = _scopeFactory.CreateScope())
                    {
                        await using (var db = scope.ServiceProvider.GetRequiredService<MonitoringDbContext>())
                        {
                            var bucketsToUpdate = new HashSet<long>();
                            var taskAnnouncementsToMake = new Dictionary<string, (string, TaskInfo)>();

                            foreach (var taskInfo in batch)
                            {
                                var runId = taskInfo.LatestStatus?.RunId ?? taskInfo.Results?.RunId;
                                var jobId = taskInfo.LatestStatus?.JobId ?? taskInfo.Results?.JobId;
                                var taskId = taskInfo.LatestStatus?.TaskId ?? taskInfo.Results?.TaskId;

                                var run = db.Runs.Find(runId);
                                if (run == null)
                                {
                                    run = new RunEntity
                                    {
                                        Id = runId,
                                        FirstSeen = DateTimeOffset.UtcNow,
                                    };
                                    db.Runs.Add(run);
                                }

                                run.LastUpdate = DateTimeOffset.UtcNow;

                                var job = db.Jobs.Find(runId, jobId);
                                if (job == null)
                                {
                                    job = new JobEntity
                                    {
                                        Id = jobId,
                                        RunId = runId,
                                        FirstSeen = DateTimeOffset.UtcNow,
                                    };
                                    db.Jobs.Add(job);
                                }

                                var task = db.Tasks.Find(runId, jobId, taskId);
                                if (task == null)
                                {
                                    task = new TaskEntity
                                    {
                                        Id = taskId,
                                        RunId = runId,
                                        JobId = jobId,
                                        FirstSeen = DateTimeOffset.UtcNow,
                                        StatsBucketId = (long)Math.Floor((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds) / 60,
                                    };
                                    db.Tasks.Add(task);
                                }

                                if (taskInfo.LatestStatus != null)
                                {
                                    task.CurrentStepName = taskInfo.LatestStatus.StepName;
                                    task.CurrentStatus = taskInfo.LatestStatus.CurrentStatus;
                                    task.CurrentDetail = taskInfo.LatestStatus.CurrentDetail;
                                    if (!string.IsNullOrWhiteSpace(taskInfo.LatestStatus.UserId))
                                    {
                                        task.UserId = taskInfo.LatestStatus.UserId;
                                    }
                                    if (!string.IsNullOrWhiteSpace(taskInfo.LatestStatus.PartyId))
                                    {
                                        task.PartyId = taskInfo.LatestStatus.PartyId;
                                    }
                                }

                                if (taskInfo.Results != null)
                                {
                                    task.HasResult = true;
                                    task.ResultIsSuccess = taskInfo.Results.IsSuccess;
                                    task.ResultIsFilled = taskInfo.Results.IsFilled;
                                    task.ResultMatchFillRate = taskInfo.Results.MatchFillRate;
                                    task.ResultTeams = taskInfo.Results.Teams.Select(x => new TaskInfoTeam
                                    {
                                        Slots = x.Slots.Select(y => new TaskInfoTeamSlot
                                        {
                                            Type = y.Type,
                                            UserId = y.UserId,
                                        }).ToArray(),
                                    }).ToArray();
                                    task.ResultCriticalErrorMessage = taskInfo.Results.CriticalErrorMessage;
                                    task.ResultRecentLines = taskInfo.Results.RecentLines;
                                    task.ResultsReceived = DateTimeOffset.UtcNow;

                                    if (task.ResultIsSuccess && task.ErrorCount == 0 && task.WarningCount == 0)
                                    {
                                        // We don't get a step update on completion, so wipe out these values if we completed successfully.
                                        task.CurrentStepName = null;
                                        task.CurrentStatus = null;
                                        task.CurrentDetail = null;
                                    }
                                }

                                bucketsToUpdate.Add(task.StatsBucketId);
                                taskAnnouncementsToMake[runId + ":" + jobId + ":" + taskId] = (task.HasResult ? "TaskResults" : "TaskProgress", task.ToTaskInfo());
                            }

                            await db.SaveChangesAsync();

                            stopwatch.Stop();
                            if (stopwatch.ElapsedMilliseconds > 100)
                            {
                                _logger.LogInformation($"Flushed {batch.Count} task updates to database in {stopwatch.ElapsedMilliseconds}ms.");
                            }

                            var reader = scope.ServiceProvider.GetRequiredService<ITaskReader>();

                            stopwatch.Restart();

                            await _hubContext.Clients.All.SendAsync("UpdateGlobalStats", reader.GetGlobalStats());
                            foreach (var statsBucket in bucketsToUpdate)
                            {
                                await _hubContext.Clients.All.SendAsync("UpdateStatsBucket", statsBucket, reader.GetStreamingStatsForBucketId(statsBucket));
                            }
                            foreach (var announcement in taskAnnouncementsToMake)
                            {
                                await _hubContext.Clients.All.SendAsync(announcement.Value.Item1, announcement.Value.Item2.RunId, announcement.Value.Item2.JobId, announcement.Value.Item2.TaskId, announcement.Value.Item2);
                            }

                            stopwatch.Stop();

                            if (stopwatch.ElapsedMilliseconds > 100)
                            {
                                _logger.LogInformation($"Announced {bucketsToUpdate.Count} bucket updates and {taskAnnouncementsToMake.Count} task updates to WebSockets in {stopwatch.ElapsedMilliseconds}ms.");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"Lost {batch.Count} task updates because an exception occurred: {ex.Message}");
                }
                finally
                {
                    batch.Clear();
                }
            }
        }
    }
}
