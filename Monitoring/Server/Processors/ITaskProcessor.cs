﻿using Monitoring;
using Server.Database;
using System.Collections.Generic;

namespace Server.Processors
{
    public interface ITaskProcessor
    {
        void EnqueueProgress(TaskProgressRequest request);
        void EnqueueResults(TaskResultsRequest request);
    }
}
