﻿using Monitoring;

namespace Server.Processors
{
    public interface ILogProcessor
    {
        void EnqueueLog(LogRequest request);
    }
}