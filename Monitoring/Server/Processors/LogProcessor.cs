﻿namespace Server.Processors
{
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using Monitoring;
    using Server.Database;
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Threading.Tasks;

    public class LogProcessor : ILogProcessor, IHostedService
    {
        private readonly ILogger<LogProcessor> _logger;
        private readonly IServiceScopeFactory _scopeFactory;
        private ConcurrentQueue<LogEntity> _pendingLogs;
        private ConcurrentQueue<IncData> _pendingIncrements;
        private Task _backgroundTask;
        private bool _isRunning;

        private static readonly Regex _heartbeatedLobbyRegex = new Regex("Heartbeating matchmaking lobby ([a-f0-9]+)", RegexOptions.Compiled);

        private class IncData
        {
            public string RunId;
            public string JobId;
            public string TaskId;
            public int Errors;
            public int Warnings;
        }

        public LogProcessor(ILogger<LogProcessor> logger, IServiceScopeFactory scopeFactory)
        {
            _pendingLogs = new ConcurrentQueue<LogEntity>();
            _pendingIncrements = new ConcurrentQueue<IncData>();
            _logger = logger;
            _scopeFactory = scopeFactory;
        }

        public void EnqueueLog(LogRequest request)
        {
            var match = _heartbeatedLobbyRegex.Match(request.Message);
            var heartbeatedLobbyId = string.Empty;
            if (match.Success)
            {
                heartbeatedLobbyId = match.Groups[1].Value;
            }

            _pendingLogs.Enqueue(new LogEntity
            {
                Bucket = request.RunId + ":" + request.JobId,
                RunId = request.RunId,
                JobId = request.JobId,
                TaskId = string.Empty,
                Timestamp = DateTimeOffset.UtcNow,
                ReportedTimestamp = request.Timestamp,
                Category = request.Category,
                Severity = request.Severity,
                StepName = request.StepName,
                Message = request.Message,
                HeartbeatedLobbyId = heartbeatedLobbyId,
            });

            if (!string.IsNullOrWhiteSpace(request.TaskId))
            {
                _pendingLogs.Enqueue(new LogEntity
                {
                    Bucket = request.RunId + ":" + request.JobId + ":" + request.TaskId,
                    RunId = request.RunId,
                    JobId = request.JobId,
                    TaskId = request.TaskId,
                    Timestamp = DateTimeOffset.UtcNow,
                    ReportedTimestamp = request.Timestamp,
                    Category = request.Category,
                    Severity = request.Severity,
                    StepName = request.StepName,
                    Message = request.Message,
                    HeartbeatedLobbyId = heartbeatedLobbyId,
                });
            }
        }

        private async Task IngestIncrements()
        {
            var batch = new List<IncData>();
            var sinceLastFlush = Stopwatch.StartNew();
            while (_isRunning)
            {
                IncData nextInc;
                if (!_pendingIncrements.TryDequeue(out nextInc))
                {
                    await Task.Yield();
                    continue;
                }

                batch.Add(nextInc);

                if (batch.Count > 25 || sinceLastFlush.ElapsedMilliseconds > 1000)
                {
                    await ProcessIncrements(batch);
                }
            }

            if (batch.Count > 0)
            {
                await ProcessIncrements(batch);
            }
        }

        private async Task ProcessIncrements(List<IncData> batch)
        {
            var stopwatch = Stopwatch.StartNew();
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    await using (var db = scope.ServiceProvider.GetRequiredService<MonitoringDbContext>())
                    {
                        foreach (var inc in batch)
                        {
                            var task = await db.FindAsync<TaskEntity>(inc.RunId, inc.JobId, inc.TaskId);
                            if (task == null)
                            {
                                _logger.LogWarning($"Needed to increment warning/error count, but couldn't find task {inc.RunId}:{inc.JobId}:{inc.TaskId}.");
                                continue;
                            }

                            task.ErrorCount += inc.Errors;
                            task.WarningCount += inc.Warnings;
                            _logger.LogInformation($"Recorded that task {inc.RunId}:{inc.JobId}:{inc.TaskId} had warnings/errors.");
                        }

                        await db.SaveChangesAsync();
                    }
                }
                _logger.LogInformation($"Flushed batch of {batch.Count} increments to database in {stopwatch.ElapsedMilliseconds}ms. Pending increment queue is at {_pendingIncrements.Count} log entities.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Lost {batch.Count} increments because an exception occurred: {ex.Message}");
            }
            finally
            {
                batch.Clear();
            }
        }

        private async Task IngestLogs()
        {
            var batch = new List<LogEntity>();
            while (_isRunning)
            {
                LogEntity nextEntity;
                if (!_pendingLogs.TryDequeue(out nextEntity))
                {
                    await Task.Yield();
                    continue;
                }

                batch.Add(nextEntity);

                if (batch.Count > 2500)
                {
                    await ProcessBatch(batch);
                }
            }

            if (batch.Count > 0)
            {
                await ProcessBatch(batch);
            }
        }

        private async Task ProcessBatch(List<LogEntity> batch)
        {
            var stopwatch = Stopwatch.StartNew();
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    await using (var db = scope.ServiceProvider.GetRequiredService<MonitoringDbContext>())
                    {
                        var incs = new Dictionary<string, IncData>();

                        foreach (var entry in batch)
                        {
                            if (!string.IsNullOrWhiteSpace(entry.TaskId) &&
                                (entry.Severity == "Warning" ||
                                entry.Severity == "Error"))
                            {
                                var fqid = entry.RunId + ":" + entry.JobId + ":" + entry.TaskId;
                                if (!incs.ContainsKey(fqid))
                                {
                                    incs.Add(fqid, new IncData
                                    {
                                        RunId = entry.RunId,
                                        JobId = entry.JobId,
                                        TaskId = entry.TaskId,
                                    });
                                }

                                if (entry.Severity == "Warning")
                                {
                                    incs[fqid].Warnings++;
                                }
                                if (entry.Severity == "Error")
                                {
                                    incs[fqid].Errors++;
                                }
                            }
                        }

                        foreach (var kv in incs)
                        {
                            _pendingIncrements.Enqueue(kv.Value);
                        }

                        await db.AddRangeAsync(batch);
                        await db.SaveChangesAsync();
                    }
                }
                _logger.LogInformation($"Flushed batch of {batch.Count} log entities to database in {stopwatch.ElapsedMilliseconds}ms. Pending log queue is at {_pendingLogs.Count} log entities.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Lost {batch.Count} log messages because an exception occurred: {ex.Message}");
            }
            finally
            {
                batch.Clear();
            }
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _backgroundTask = Task.WhenAll(
                Task.Run(IngestLogs),
                Task.Run(IngestLogs),
                Task.Run(IngestLogs),
                Task.Run(IngestLogs),
                Task.Run(IngestIncrements)
            );
            _isRunning = true;
            return Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _isRunning = false;
            await _backgroundTask;
        }
    }
}
