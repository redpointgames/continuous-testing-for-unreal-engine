﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Server.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Jobs",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    RunId = table.Column<string>(type: "text", nullable: false),
                    FirstSeen = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Jobs", x => new { x.RunId, x.Id });
                });

            migrationBuilder.CreateTable(
                name: "Logs",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Bucket = table.Column<string>(type: "text", nullable: true),
                    RunId = table.Column<string>(type: "text", nullable: true),
                    JobId = table.Column<string>(type: "text", nullable: true),
                    TaskId = table.Column<string>(type: "text", nullable: true),
                    Timestamp = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    ReportedTimestamp = table.Column<string>(type: "text", nullable: true),
                    Category = table.Column<string>(type: "text", nullable: true),
                    Severity = table.Column<string>(type: "text", nullable: true),
                    StepName = table.Column<string>(type: "text", nullable: true),
                    Message = table.Column<string>(type: "text", nullable: true),
                    HeartbeatedLobbyId = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Runs",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    FirstSeen = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    LastUpdate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Runs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    RunId = table.Column<string>(type: "text", nullable: false),
                    JobId = table.Column<string>(type: "text", nullable: false),
                    FirstSeen = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    ResultsReceived = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    StatsBucketId = table.Column<long>(type: "bigint", nullable: false),
                    UserId = table.Column<string>(type: "text", nullable: true),
                    PartyId = table.Column<string>(type: "text", nullable: true),
                    CurrentStepName = table.Column<string>(type: "text", nullable: true),
                    CurrentStatus = table.Column<string>(type: "text", nullable: true),
                    CurrentDetail = table.Column<string>(type: "text", nullable: true),
                    CurrentProgress = table.Column<float>(type: "real", nullable: false),
                    CurrentETA = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    WarningCount = table.Column<int>(type: "integer", nullable: false),
                    ErrorCount = table.Column<int>(type: "integer", nullable: false),
                    HasResult = table.Column<bool>(type: "boolean", nullable: false),
                    ResultIsSuccess = table.Column<bool>(type: "boolean", nullable: false),
                    ResultIsFilled = table.Column<bool>(type: "boolean", nullable: false),
                    ResultMatchFillRate = table.Column<float>(type: "real", nullable: false),
                    ResultTeams = table.Column<string>(type: "text", nullable: true),
                    ResultCriticalErrorMessage = table.Column<string>(type: "text", nullable: true),
                    ResultRecentLines = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => new { x.RunId, x.JobId, x.Id });
                });

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_RunId",
                table: "Jobs",
                column: "RunId");

            migrationBuilder.CreateIndex(
                name: "IX_Logs_Bucket",
                table: "Logs",
                column: "Bucket");

            migrationBuilder.CreateIndex(
                name: "IX_Logs_HeartbeatedLobbyId",
                table: "Logs",
                column: "HeartbeatedLobbyId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_RunId_JobId",
                table: "Tasks",
                columns: new[] { "RunId", "JobId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Jobs");

            migrationBuilder.DropTable(
                name: "Logs");

            migrationBuilder.DropTable(
                name: "Runs");

            migrationBuilder.DropTable(
                name: "Tasks");
        }
    }
}
