﻿using Server.Database;
using System.Collections.Generic;

namespace Server.Readers
{
    public interface ITaskReader
    {
        (RunInfo[] runs, JobInfo[] jobs, TaskInfo[] tasks) GetCurrentStateForInit();
        GlobalStats GetGlobalStats();
        string GetLogs(string id);
        Dictionary<long, StreamingStats> GetStreamingStats();
        StreamingStats GetStreamingStatsForBucketId(long bucketId);
    }
}
