﻿namespace Server.Readers
{
    using Microsoft.EntityFrameworkCore;
    using Server.Database;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class TaskReader : ITaskReader
    {
        private readonly MonitoringDbContext _db;

        public TaskReader(MonitoringDbContext db)
        {
            _db = db;
        }

        public (RunInfo[] runs, JobInfo[] jobs, TaskInfo[] tasks) GetCurrentStateForInit()
        {
            var threshold = DateTimeOffset.UtcNow.AddDays(-1);

            var tasks =
                _db.Tasks
                    .Where(task => !(task.HasResult && task.ResultIsSuccess && task.ResultIsFilled && task.ErrorCount == 0 && task.WarningCount == 0))
                    .OrderByDescending(task => task.FirstSeen)
                    .AsEnumerable()
                    .Where(x => x.FirstSeen >= threshold)
                    .Select(x => x.ToTaskInfo())
                    .ToArray();

            var runCounts = new Dictionary<string, long>();
            var jobCounts = new Dictionary<(string runId, string jobId), long>();
            foreach (var task in tasks)
            {
                if (!runCounts.ContainsKey(task.RunId))
                {
                    runCounts.Add(task.RunId, 0);
                }
                if (!jobCounts.ContainsKey((task.RunId, task.JobId)))
                {
                    jobCounts.Add((task.RunId, task.JobId), 0);
                }
                runCounts[task.RunId]++;
                jobCounts[(task.RunId, task.JobId)]++;
            }

            return (
                _db.Runs
                    .OrderByDescending(x => x.FirstSeen)
                    .AsEnumerable()
                    .Where(x => runCounts.ContainsKey(x.Id))
                    .Select(x => new RunInfo
                    {
                        RunId = x.Id,
                        FirstSeenUtc = x.FirstSeen.ToUnixTimeSeconds(),
                    })
                    .ToArray(),
                _db.Jobs
                    .OrderByDescending(task => task.FirstSeen)
                    .AsEnumerable()
                    .Where(x => jobCounts.ContainsKey((x.RunId, x.Id)))
                    .Select(x => new JobInfo
                    {
                        RunId = x.RunId,
                        JobId = x.Id,
                        FirstSeenUtc = x.FirstSeen.ToUnixTimeSeconds(),
                    })
                    .ToArray(),
                tasks
            );
        }

        public StreamingStats GetStreamingStatsForBucketId(long bucketId)
        {
            var v = _db.BucketStreamingStats.Find(bucketId);
            if (v == null)
            {
                return new StreamingStats();
            }
            return new StreamingStats
            {
                SuccessfulFills = v.SuccessfulFills,
                SuccessfulPartials = v.SuccessfulPartials,
                WithLogWarnings = v.WithLogWarnings,
                WithLogErrors = v.WithLogErrors,
                WithCriticalErrors = v.WithCriticalErrors,
                InProgress = v.InProgress,
            };
        }

        public GlobalStats GetGlobalStats()
        {
            return _db.GlobalStats.First();
        }

        public string GetLogs(string id)
        {
            var builder = new StringBuilder();
            var foundLines = false;
            foreach (var line in _db.Logs.Where(x => x.Bucket == id).OrderBy(x => x.Timestamp))
            {
                builder.Append(line.ReportedTimestamp.PadRight(24));
                builder.Append(line.Severity.PadRight(15));
                builder.AppendLine(line.Message);
                foundLines = true;
            }
            if (!foundLines)
            {
                builder.Append("No data or task/job doesn't exist.");
            }
            return builder.ToString();
        }

        public Dictionary<long, StreamingStats> GetStreamingStats()
        {
            var threshold = DateTimeOffset.UtcNow.AddDays(-1).ToUnixTimeSeconds() / 60;
            return _db.BucketStreamingStats
                .Where(x => x.StatsBucketId >= threshold)
                .ToDictionary(
                    k => k.StatsBucketId,
                    v => new StreamingStats
                    {
                        SuccessfulFills = v.SuccessfulFills,
                        SuccessfulPartials = v.SuccessfulPartials,
                        WithLogWarnings = v.WithLogWarnings,
                        WithLogErrors = v.WithLogErrors,
                        WithCriticalErrors = v.WithCriticalErrors,
                        InProgress = v.InProgress,
                    });
        }
    }
}
