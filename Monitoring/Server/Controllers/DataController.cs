﻿namespace Server.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Server.Database;
    using System.Threading.Tasks;

    [ApiController]
    public class DataController : ControllerBase
    {
        private readonly MonitoringDbContext _db;

        public DataController(MonitoringDbContext db)
        {
            _db = db;
        }

        [HttpPost]
        [Route("/api/clear")]
        public async Task<IActionResult> ClearAllData()
        {
            await _db.Database.ExecuteSqlRawAsync($"TRUNCATE \"public\".\"{_db.Model.FindEntityType(typeof(RunEntity)).GetTableName()}\"");
            await _db.Database.ExecuteSqlRawAsync($"TRUNCATE \"public\".\"{_db.Model.FindEntityType(typeof(JobEntity)).GetTableName()}\"");
            await _db.Database.ExecuteSqlRawAsync($"TRUNCATE \"public\".\"{_db.Model.FindEntityType(typeof(TaskEntity)).GetTableName()}\"");
            await _db.Database.ExecuteSqlRawAsync($"TRUNCATE \"public\".\"{_db.Model.FindEntityType(typeof(LogEntity)).GetTableName()}\"");
            await _db.SaveChangesAsync();
            return Ok();
        }
    }
}
