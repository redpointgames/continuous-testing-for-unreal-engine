﻿namespace Server.Controllers
{
    using Grpc.Core;
    using Microsoft.Extensions.Logging;
    using Monitoring;
    using Server.Processors;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class MonitoringRpcService : Monitoring.MonitoringRpc.MonitoringRpcBase
    {
        private readonly ILogger<MonitoringRpcService> _logger;
        private readonly ILogProcessor _logProcessor;
        private readonly ITaskProcessor _taskProcessor;
        private readonly EmptyResponse _emptyResponse;

        public MonitoringRpcService(ILogger<MonitoringRpcService> logger, ILogProcessor logProcessor, ITaskProcessor taskProcessor)
        {
            _logger = logger;
            _logProcessor = logProcessor;
            _taskProcessor = taskProcessor;
            _emptyResponse = new EmptyResponse();
        }

        public override Task<PingResponse> Ping(PingRequest request, ServerCallContext context)
        {
            return Task.FromResult(new PingResponse
            {
                ServerMessage = $"Hello {request.RunId}:{request.JobId}, you are connected to the monitoring server."
            });
        }

        public override Task<EmptyResponse> Log(LogRequest request, ServerCallContext context)
        {
            try
            {
                _logProcessor.EnqueueLog(request);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
            return Task.FromResult(_emptyResponse);
        }

        public override Task<EmptyResponse> TaskProgress(TaskProgressRequest request, ServerCallContext context)
        {
            try
            {
                _taskProcessor.EnqueueProgress(request);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
            return Task.FromResult(_emptyResponse);
        }

        public override Task<EmptyResponse> TaskResults(TaskResultsRequest request, ServerCallContext context)
        {
            try
            {
                _taskProcessor.EnqueueResults(request);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
            return Task.FromResult(_emptyResponse);
        }
    }
}
