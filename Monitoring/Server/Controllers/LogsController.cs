﻿namespace Server.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Server.Database;
    using Server.Readers;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    [ApiController]
    public class LogsController : ControllerBase
    {
        private readonly ITaskReader _reader;
        private readonly MonitoringDbContext _db;

        public LogsController(ITaskReader reader, MonitoringDbContext db)
        {
            _reader = reader;
            _db = db;
        }

        [HttpGet]
        [Route("/api/logs/fixed/{id}")]
        public string FixedLogs([FromRoute] string id)
        {
            return _reader.GetLogs(id);
        }

        [HttpGet]
        [Route("/api/logs/heartbeater/{id}")]
        public string HeartbeaterLogs([FromRoute] string id)
        {
            var discoveredLog = _db.Logs.FirstOrDefault(x => x.HeartbeatedLobbyId == id);
            if (discoveredLog == null)
            {
                return "No logs could be found for the responsible heartbeater.";
            }
            return "Reading logs from bucket " + discoveredLog.Bucket + "\n" + _reader.GetLogs(discoveredLog.Bucket);
        }
    }
}
