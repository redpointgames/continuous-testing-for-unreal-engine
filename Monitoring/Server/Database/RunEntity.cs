﻿namespace Server.Database
{
    using System;

    // [Table("runs")]
    public class RunEntity
    {
        public string Id { get; set; }

        public DateTimeOffset FirstSeen { get; set; }
        public DateTimeOffset LastUpdate { get; set; }
    }
}
