﻿namespace Server.Database
{
    using Microsoft.EntityFrameworkCore;

    [Keyless]
    public class BucketStreamingStats
    {
        public long StatsBucketId { get; set; }
        public int? SuccessfulFills { get; set; }
        public int? SuccessfulPartials { get; set; }
        public int? WithLogWarnings { get; set; }
        public int? WithLogErrors { get; set; }
        public int? WithCriticalErrors { get; set; }
        public int? InProgress { get; set; }
    }
}
