﻿namespace Server.Database
{
    using Microsoft.EntityFrameworkCore;
    using System;

    [Index(nameof(HeartbeatedLobbyId))]
    [Index(nameof(Bucket))]
    public class LogEntity
    {
        public long Id { get; set; }

        public string Bucket { get; set; }

        public string RunId { get; set; }
        public string JobId { get; set; }
        public string TaskId { get; set; }

        public DateTimeOffset Timestamp { get; set; }
        public string ReportedTimestamp { get; set; }
        public string Category { get; set; }
        public string Severity { get; set; }
        public string StepName { get; set; }
        public string Message { get; set; }

        public string HeartbeatedLobbyId { get; set; }
    }
}
