﻿namespace Server.Database
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Threading.Tasks;

    public class MonitoringDbContext : DbContext
    {
        public DbSet<RunEntity> Runs { get; set; }
        public DbSet<JobEntity> Jobs { get; set; }
        public DbSet<TaskEntity> Tasks { get; set; }
        public DbSet<LogEntity> Logs { get; set; }
        public DbSet<GlobalStats> GlobalStats { get; set; }
        public DbSet<BucketStreamingStats> BucketStreamingStats { get; set; }

        public MonitoringDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RunEntity>();
            modelBuilder.Entity<JobEntity>().HasKey(c => new { c.RunId, c.Id });
            modelBuilder.Entity<TaskEntity>()
                .HasKey(c => new { c.RunId, c.JobId, c.Id });
            modelBuilder.Entity<TaskEntity>().Property(e => e.ResultTeams).HasConversion(
                    v => JsonConvert.SerializeObject(v, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }),
                    v => JsonConvert.DeserializeObject<TaskInfoTeam[]>(v, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));
            modelBuilder.Entity<LogEntity>();

            modelBuilder.Entity<GlobalStats>(
                eb =>
                {
                    eb.HasNoKey();
                    eb.ToSqlQuery(@"
SELECT AVG(""ResultMatchFillRate"") AS ""AverageMatchFillRate"", COUNT(""Id"") AS ""PlayersMatched"", AVG(EXTRACT(SECONDS FROM ""ResultsReceived"" - ""FirstSeen"")) AS ""AverageMatchTimeSeconds""
FROM ""Tasks""
WHERE ""ResultsReceived"" IS NOT NULL AND ""HasResult"" = TRUE AND ""ResultIsSuccess"" IS TRUE
");
                    eb.ToTable("GlobalStats", t => t.ExcludeFromMigrations());
                });
            modelBuilder.Entity<BucketStreamingStats>(
                eb =>
                {
                    eb.HasKey(x => x.StatsBucketId);
                    eb.ToSqlQuery(@"
SELECT
	""StatsBucketId"", 
	COUNT(""Id"") FILTER (
		WHERE ""HasResult"" = FALSE
	) AS ""InProgress"", 
	COUNT(""Id"") FILTER (
		WHERE ""HasResult"" = TRUE 
		AND ""ResultIsSuccess"" = FALSE
	) AS ""WithCriticalErrors"", 
	COUNT(""Id"") FILTER (
		WHERE ""HasResult"" = TRUE 
		AND ""ResultIsSuccess"" = TRUE
		AND ""ErrorCount"" > 0
	) AS ""WithLogErrors"", 
	COUNT(""Id"") FILTER (
		WHERE ""HasResult"" = TRUE 
		AND ""ResultIsSuccess"" = TRUE
		AND ""ErrorCount"" = 0
		AND ""WarningCount"" > 0
	) AS ""WithLogWarnings"", 
	COUNT(""Id"") FILTER (
		WHERE ""HasResult"" = TRUE 
		AND ""ResultIsSuccess"" = TRUE
		AND ""ErrorCount"" = 0
		AND ""WarningCount"" = 0
		AND ""ResultIsFilled"" = FALSE
	) AS ""SuccessfulPartials"", 
	COUNT(""Id"") FILTER (
		WHERE ""HasResult"" = TRUE 
		AND ""ResultIsSuccess"" = TRUE
		AND ""ErrorCount"" = 0
		AND ""WarningCount"" = 0
		AND ""ResultIsFilled"" = TRUE
	) AS ""SuccessfulFills""
FROM ""Tasks""
GROUP BY ""StatsBucketId""
");
                    eb.ToTable("BucketStreamingStats", t => t.ExcludeFromMigrations());
                });
        }
    }
}
