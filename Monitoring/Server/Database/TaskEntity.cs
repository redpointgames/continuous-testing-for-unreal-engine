﻿namespace Server.Database
{
    using Microsoft.EntityFrameworkCore;
    using System;

    [Index(nameof(RunId), nameof(JobId))]
    public class TaskEntity
    {
        public string Id { get; set; }
        public string RunId { get; set; }
        public string JobId { get; set; }

        public string FullyQualifiedId => RunId + ":" + JobId + ":" + Id;

        public DateTimeOffset FirstSeen { get; set; }
        public DateTimeOffset ResultsReceived { get; set; }
        public long StatsBucketId { get; set; }

        public string UserId { get; set; }
        public string PartyId { get; set; }

        public string CurrentStepName { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentDetail { get; set; }
        public float CurrentProgress { get; set; }
        public DateTimeOffset? CurrentETA { get; set; }

        public int WarningCount { get; set; }
        public int ErrorCount { get; set; }

        public bool HasResult { get; set; }
        public bool ResultIsSuccess { get; set; }
        public bool ResultIsFilled { get; set; }
        public float ResultMatchFillRate { get; set; }
        public TaskInfoTeam[] ResultTeams { get; set; }
        public string ResultCriticalErrorMessage { get; set; }
        public string ResultRecentLines { get; set; }

        public TaskInfo ToTaskInfo()
        {
            return new TaskInfo
            {
                FirstSeenUtc = FirstSeen.ToUnixTimeSeconds(),
                DurationSeconds = ResultsReceived > FirstSeen ? (long)Math.Ceiling((ResultsReceived - FirstSeen).TotalSeconds) : null,
                RunId = RunId,
                JobId = JobId,
                TaskId = Id,
                StepName = CurrentStepName,
                CurrentStatus = CurrentStatus,
                CurrentDetail = CurrentDetail,
                CurrentProgress = CurrentProgress,
                ETATimestamp = CurrentETA.ToString(), // TODO
                WarningCount = WarningCount,
                ErrorCount = ErrorCount,
                UserId = UserId,
                PartyId = PartyId,
                IsSuccess = HasResult ? ResultIsSuccess : null,
                IsFilled = HasResult ? ResultIsFilled : null,
                MatchFillRate = HasResult ? ResultMatchFillRate : null,
                Teams = HasResult ? ResultTeams : null,
                CriticalErrorMessage = HasResult ? ResultCriticalErrorMessage : null,
                RecentLines = HasResult ? ResultRecentLines : null,
            };
        }
    }
}
