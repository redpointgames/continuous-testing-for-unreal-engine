﻿namespace Server.Database
{
    using Microsoft.EntityFrameworkCore;
    using System;

    [Index(nameof(RunId))]
    public class JobEntity
    {
        public string Id { get; set; }
        public string RunId { get; set; }

        public string FullyQualifiedId => RunId + ":" + Id;

        public DateTimeOffset FirstSeen { get; set; }
    }
}
