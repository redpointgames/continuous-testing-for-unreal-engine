﻿namespace Server
{
    public class JobInfo
    {
        public string RunId { get; set; }
        public string JobId { get; set; }
        public long FirstSeenUtc { get; set; }
    }
}
