﻿namespace Server
{
    public class RunInfo
    {
        public string RunId { get; set; }
        public long FirstSeenUtc { get; set; }
    }
}
