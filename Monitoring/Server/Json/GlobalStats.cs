﻿using Microsoft.EntityFrameworkCore;

namespace Server
{
    [Keyless]
    public class GlobalStats
    {
        public int? PlayersMatched { get; set; }
        public double? AverageMatchFillRate { get; set; }
        public double? AverageMatchTimeSeconds { get; set; }
    }
}
