﻿using Newtonsoft.Json;

namespace Server
{
    public class TaskInfoTeamSlot
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("user_id")]
        public string UserId { get; set; }
    }
}