﻿using Newtonsoft.Json;

namespace Server
{
    public class TaskInfoTeam
    {
        [JsonProperty("slots")]
        public TaskInfoTeamSlot[] Slots { get; set; }
    }
}
