﻿using Newtonsoft.Json;

namespace Server
{
    public class TaskInfo
    {
        public long FirstSeenUtc { get; set; }

        public long? DurationSeconds { get; set; }

        [JsonProperty("message_type")]
        public string MessageType { get; set; }

        [JsonProperty("run_id")]
        public string RunId { get; set; }

        [JsonProperty("job_id")]
        public string JobId { get; set; }

        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }

        [JsonProperty("category")]
        public string Category { get; set; }

        [JsonProperty("severity")]
        public string Severity { get; set; }

        [JsonProperty("task_id")]
        public string TaskId { get; set; }

        [JsonProperty("step_name")]
        public string StepName { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("is_success")]
        public bool? IsSuccess { get; set; }

        [JsonProperty("is_filled")]
        public bool? IsFilled { get; set; }

        [JsonProperty("match_fill_rate")]
        public float? MatchFillRate { get; set; }

        [JsonProperty("teams")]
        public TaskInfoTeam[] Teams { get; set; }

        [JsonProperty("user_id")]
        public string UserId { get; set; }

        [JsonProperty("party_id")]
        public string PartyId { get; set; }

        [JsonProperty("current_status")]
        public string CurrentStatus { get; set; }

        [JsonProperty("current_detail")]
        public string CurrentDetail { get; set; }

        [JsonProperty("current_progress")]
        public float CurrentProgress { get; set; }

        [JsonProperty("eta_timestamp")]
        public string ETATimestamp { get; set; }

        [JsonProperty("critical_error_message")]
        public string CriticalErrorMessage { get; set; }

        [JsonProperty("recent_lines")]
        public string RecentLines { get; set; }

        public int WarningCount { get; set; }

        public int ErrorCount { get; set; }
    }
}
