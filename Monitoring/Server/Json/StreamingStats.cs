﻿using Microsoft.EntityFrameworkCore;

namespace Server
{
    public class StreamingStats
    {
        public int? SuccessfulFills { get; set; }
        public int? SuccessfulPartials { get; set; }
        public int? WithLogWarnings { get; set; }
        public int? WithLogErrors { get; set; }
        public int? WithCriticalErrors { get; set; }
        public int? InProgress { get; set; }
    }
}
