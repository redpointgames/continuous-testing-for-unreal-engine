using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Server.Controllers;
using Server.Database;
using Server.Processors;
using Server.Readers;
using System;

namespace Server
{
    public class Startup
    {
        private readonly IHostEnvironment _hostEnvironment;

        public Startup(IConfiguration configuration, IHostEnvironment hostEnvironment)
        {
            Configuration = configuration;
            _hostEnvironment = hostEnvironment;
        }

        public IConfiguration Configuration { get; }

        private static ILoggerFactory ContextLoggerFactory
            => LoggerFactory.Create(b => b.AddConsole().AddFilter("", LogLevel.Information));

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSignalR();
            services.AddGrpc();

            // Register the log processor, which accepts logs and then flushes them to
            // Postgres in batches.
            services.AddSingleton<LogProcessor, LogProcessor>();
            services.AddSingleton<ILogProcessor>(x => x.GetRequiredService<LogProcessor>());
            services.AddHostedService(x => x.GetRequiredService<LogProcessor>());

            // Register the task processor, which accepts updates and results for tasks and then
            // flushes them to Postgres in batches. It also queues up notifications to be sent
            // out to clients connected to the monitoring WebSockets.
            services.AddSingleton<TaskProcessor, TaskProcessor>();
            services.AddSingleton<ITaskProcessor>(x => x.GetRequiredService<TaskProcessor>());
            services.AddHostedService(x => x.GetRequiredService<TaskProcessor>());

            // Register the reader which is used to grab tasks and logs.
            services.AddScoped<ITaskReader, TaskReader>();

            // Register the Postgres database connection, which is used for log storage.
            string connectionString;
            if (_hostEnvironment.IsProduction())
            {
                connectionString = $"Host={Environment.GetEnvironmentVariable("DB_PGSQL_HOSTNAME")};Database=monitoring;Username={Environment.GetEnvironmentVariable("DB_PGSQL_USERNAME")};Password={Environment.GetEnvironmentVariable("DB_PGSQL_PASSWORD")}";
            }
            else
            {
                connectionString = Configuration.GetConnectionString("DefaultConnection");
            }
            services.AddDbContext<MonitoringDbContext>(options => options
                //.UseLoggerFactory(ContextLoggerFactory)
                .UseNpgsql(connectionString));

            services.AddControllersWithViews();

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, MonitoringDbContext db)
        {
            db.Database.Migrate();

            // We know we're always behind HTTPS.
            app.Use((context, next) =>
            {
                context.Request.Scheme = "https";
                return next();
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            // app.UseGrpcWeb();

            app.UseWebSockets();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}").RequireHost("*:5000", "*:443");

                endpoints.MapHub<MatchmakingDataHub>("/hub").RequireHost("*:5000", "*:443");

                endpoints.MapGrpcService<MonitoringRpcService>().RequireHost("*:6414");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}
