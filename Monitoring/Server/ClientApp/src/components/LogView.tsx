import React, { Component } from "react";
import { Link } from "react-router-dom";

interface LogViewProps {
    type: string;
    id: string;
}

interface LogViewState {
    data: string | null;
    loading: boolean;
}

export class LogView extends Component<LogViewProps, LogViewState> {

    constructor(props: LogViewProps) {
        super(props);
        this.state = {
            data: null,
            loading: true,
        };
    }

    componentDidMount() {
        this.loadLogs(this.props.id);
    }

    componentDidUpdate(prevProps: LogViewProps) {
        if (prevProps.id !== this.props.id) {
            this.loadLogs(this.props.id);
        }
    }

    render() {
        if (this.state.loading) {
            return <p>Loading logs...</p>
        }

        let lines = [];
        let splitData = this.state.data?.split('\n');
        if (splitData !== undefined) {
            for (let i = 0; i < splitData.length; i++) {
                if (splitData[i].indexOf("Error") !== -1) {
                    lines.push(<span key={i} className="text-danger" style={{fontWeight: "bold"}}>{splitData[i]}</span>);
                } else if (splitData[i].indexOf("Warning") !== -1) {
                    lines.push(<span key={i} className="text-warning" style={{fontWeight: "bold"}}>{splitData[i]}</span>);
                } else {
                    lines.push(<span key={i} className="">{splitData[i]}</span>);
                }
                if (i !== splitData.length - 1) {
                    lines.push("\n");
                }
            }
        }

        let linkToJobView = null;
        if ((this.props.id.match(/:/g) || []).length === 2) {
            linkToJobView = (
                <Link to={"/logs/general/" + this.props.id.substring(0, this.props.id.lastIndexOf(":"))}>View job level logs</Link>
            )
        }

        return (
            <>
                <p>
                    Logs do not live update. {linkToJobView}
                </p>
                <pre className="p-2 mt-2" style={{ 
                    whiteSpace: "pre-wrap", 
                    maxWidth: "100%",
                    background: "#333", 
                    color: "#fff", 
                    borderRadius: "5px" }}><code>{lines}</code></pre>
            </>
        );
    }

    async loadLogs(id: string) {
        this.setState({ data: null, loading: true });
        const response = await fetch('/api/logs/'+ this.props.type + '/' + id);
        const data = await response.text();
        this.setState({ data: data, loading: false });
    }

};