import React, { Component, Fragment } from "react";
import * as signalR from "@microsoft/signalr";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRunning, faCog, faBriefcase, faCheckCircle, faTimesCircle, faChevronLeft, faChevronRight, faUser, faRobot, faExpand, faQuestion, faExclamationTriangle, faTextHeight, faUsers } from '@fortawesome/free-solid-svg-icons'
import { Link } from "react-router-dom";
import * as Dimensions from "react-dimensions";
import { AlignedData, Options } from 'uplot';
import UplotReact from './monitor/UplotReact';
import 'uplot/dist/uPlot.min.css';
import { JobInfo, RunInfo, TaskInfo, TaskTree } from "../data/TaskTree";
import { TaskEntry } from "./monitor/TaskEntry";
import { JobEntry } from "./monitor/JobEntry";
import { RunEntry } from "./monitor/RunEntry";
import { GlobalStats as GlobalStatsComponent } from "./monitor/GlobalStats";

interface MonitorProps {
}

type StreamingStats = {
    withCriticalErrors: number;
    withLogErrors: number;
    withLogWarnings: number;
    successfulPartials: number;
    successfulFills: number;
    inProgress: number;
};

type GlobalStats = {
    playersMatched: number;
    averageMatchFillRate: number;
    averageMatchTimeSeconds: number;
}

interface MonitorState {
    tree: TaskTree;
    gotInitialState: boolean;
    bucketIdToStreamingIndex: { [bucketId: string]: number };
    streamingStats: AlignedData;
    globalStats: GlobalStats;
    streamingStatsDataHash: number;
}

class DimensionForward extends React.Component<any> {
    public render() {
        return this.props.f(this.props.containerWidth, this.props.containerHeight);
    }
}

const DimensionsDynamic = Dimensions()(DimensionForward);

const X_SERIES = 0;
const Y_SERIES_CRITICAL_ERRORS = 1;
const Y_SERIES_LOG_ERRORS = 2;
const Y_SERIES_LOG_WARNINGS = 3;
const Y_SERIES_PARTIAL_FILLS = 4;
const Y_SERIES_SUCCESSFUL_FILLS = 5;
const Y_SERIES_IN_PROGRESS = 6;
const chartOptions: Options = {
    width: 600,
    height: 150,
    series: [
        {},
        {
            label: "Failure",
            stroke: "#e60017",
        },
        {
            label: "With Errors",
            stroke: "#cb5d68",
        },
        {
            label: "With Warnings",
            stroke: "#ffc107",
        },
        {
            label: "Partial",
            stroke: "#007bff",
        },
        {
            label: "Filled",
            stroke: "#28a745",
        },
        {
            label: "In Progress",
            stroke: "#6c757d",
        }
    ]
};

export class Monitor extends Component<MonitorProps, MonitorState> {

    private connection: signalR.HubConnection | undefined;

    constructor(props: MonitorProps) {
        super(props);
        this.state = {
            tree: new TaskTree(),
            gotInitialState: false,
            bucketIdToStreamingIndex: {},
            streamingStats: [ [] ],
            streamingStatsDataHash: 0,
            globalStats: {
                playersMatched: 0,
                averageMatchFillRate: 0,
                averageMatchTimeSeconds: 0,
            }
        };
    }

    public componentDidMount() {
        this.connection = new signalR.HubConnectionBuilder()
            .withUrl("/hub")
            .withAutomaticReconnect()
            .build();
        this.connection.on("JobStarted", (runId: string, jobId: string) => {
        });
        this.connection.on("TaskProgress", (runId: string, jobId: string, taskId: string, task: TaskInfo) => {
            let tree = this.state.tree;
            tree.update(task);
            this.setState({tree});
        });
        this.connection.on("TaskResults", (runId: string, jobId: string, taskId: string, task: TaskInfo) => {
            let tree = this.state.tree;
            tree.update(task);
            this.setState({tree});
        });
        this.connection.on("TaskRemoved", (runId: string, jobId: string, taskId: string) => {
            let tree = this.state.tree;
            if (tree.remove(runId, jobId, taskId)) {
                this.setState({tree});
            }
        });
        this.connection.on("JobRemoved", (runId: string, jobId: string) => {
            let tree = this.state.tree;
            if (tree.removeJob(runId, jobId)) {
                this.setState({tree});
            }
        });
        this.connection.on("RunRemoved", (runId: string) => {
            let tree = this.state.tree;
            if (tree.removeRun(runId)) {
                this.setState({tree});
            }
        });
        this.connection.on("Init", (runs: RunInfo[], jobs: JobInfo[], tasks: TaskInfo[], initialStats: { [bucketId: number]: StreamingStats }, globalStats: GlobalStats) => {
            let userIds: { [fullTaskId: string]: string } = {};
            
            let tree = this.state.tree;
            tree.initFromDataSet(runs, jobs, tasks);

            let bucketIds = Object.getOwnPropertyNames(initialStats).map(v => parseInt(v));
            let bucketSet = new Set(bucketIds);
            const start = Math.min(...bucketIds);
            const end = Math.max(...bucketIds);
            for (let i = start; i < end; i++) {
                if (!bucketSet.has(i)) {
                    bucketIds.push(i);
                }
            }
            bucketIds.sort((a, b) => {
                if (a < b) {
                    return -1;
                } else if (a === b) {
                    return 0;
                } else {
                    return 1;
                }
            })

            let streamingIndex: { [bucketId: string]: number } = {};
            let streamingStats: AlignedData = [ [], [], [], [], [], [], [] ];
            let index = 0;
            for (const bucketId of bucketIds) {
                let stats = {
                    withCriticalErrors: 0,
                    withLogWarnings: 0,
                    withLogErrors: 0,
                    successfulPartials: 0,
                    successfulFills: 0,
                    inProgress: 0,
                };
                if (bucketSet.has(bucketId)) {
                    stats = initialStats[bucketId];
                }
                streamingIndex[bucketId] = index;
                streamingStats[X_SERIES][index] = bucketId * 60;
                streamingStats[Y_SERIES_CRITICAL_ERRORS][index] = stats.withCriticalErrors;
                streamingStats[Y_SERIES_LOG_ERRORS][index] = stats.withLogErrors;
                streamingStats[Y_SERIES_LOG_WARNINGS][index] = stats.withLogWarnings;
                streamingStats[Y_SERIES_PARTIAL_FILLS][index] = stats.successfulPartials;
                streamingStats[Y_SERIES_SUCCESSFUL_FILLS][index] = stats.successfulFills;
                streamingStats[Y_SERIES_IN_PROGRESS][index] = stats.inProgress;
                index++;
            }

            this.setState({ tree, gotInitialState: true, bucketIdToStreamingIndex: streamingIndex, streamingStats: streamingStats, globalStats: globalStats, streamingStatsDataHash: this.state.streamingStatsDataHash + 1 });
        });
        this.connection.on("UpdateStatsBucket", (bucketId: number, stats: StreamingStats) => {
            let streamingIndex = this.state.bucketIdToStreamingIndex;
            if (streamingIndex[bucketId] === undefined) {
                streamingIndex[bucketId] = this.state.streamingStats[0].length;
                this.setState({ bucketIdToStreamingIndex: {...streamingIndex} });
            }

            let streamingStats = this.state.streamingStats;
            streamingStats[X_SERIES][streamingIndex[bucketId]] = bucketId * 60;
            streamingStats[Y_SERIES_CRITICAL_ERRORS][streamingIndex[bucketId]] = stats.withCriticalErrors;
            streamingStats[Y_SERIES_LOG_ERRORS][streamingIndex[bucketId]] = stats.withLogErrors;
            streamingStats[Y_SERIES_LOG_WARNINGS][streamingIndex[bucketId]] = stats.withLogWarnings;
            streamingStats[Y_SERIES_PARTIAL_FILLS][streamingIndex[bucketId]] = stats.successfulPartials;
            streamingStats[Y_SERIES_SUCCESSFUL_FILLS][streamingIndex[bucketId]] = stats.successfulFills;
            streamingStats[Y_SERIES_IN_PROGRESS][streamingIndex[bucketId]] = stats.inProgress;
            this.setState({streamingStats: [...streamingStats], streamingStatsDataHash: this.state.streamingStatsDataHash + 1});
        });
        this.connection.on("UpdateGlobalStats", (stats: GlobalStats) => {
            this.setState({ globalStats: stats });
        });
        this.connection.start().catch(err => {
            console.error(err);
        })
    }

    public componentWillUnmount() {
        if (this.connection !== undefined) {
            this.connection.stop();
        }
        this.connection = undefined;
    }

    public render() {
        let rows = [];
        for (const run of this.state.tree) {
            rows.push(
                <RunEntry key={"run_" + run.key} runId={run.key} />
            );
            for (const job of run.value.jobs) {
                const showJobLevel = job.value.tasks.size() >= 2;
                if (showJobLevel) {
                    rows.push(
                        <JobEntry key={"run_" + run.key + "_job_" + job.key} jobId={job.key} />
                    );
                }
                for (const task of job.value.tasks) {
                    rows.push(
                        <TaskEntry
                            key={task.value.runId + ":" + task.value.jobId + ":" + task.value.taskId}
                            task={task.value}
                            userId={this.state.tree.getUserId(run.key, job.key, task.key)}
                            partyId={this.state.tree.getPartyId(run.key, job.key, task.key)}
                            showJobLevel={showJobLevel}
                        />
                    )
                }
            }
        }
        if (!this.state.gotInitialState) {
            rows.push(
                <tr key="loading">
                    <td colSpan={5} width="50%">
                        <FontAwesomeIcon className="mr-2" icon={faCog} spin fixedWidth />
                        <em>Connecting to monitoring server...</em>
                    </td>
                </tr>
            );
        }
        if (rows.length === 0) {
            rows.push(
                <tr key="no_runs">
                    <td colSpan={5} width="50%">
                        <em>There are no matchmaking runs recorded yet.</em>
                    </td>
                </tr>
            );
        }

        return (
            <>
                <div>
                    <DimensionsDynamic f={(width: number, height: number) => {
                        const chartOptionsUpdated = {
                            ...chartOptions,
                            width: width,
                            height: 150,
                        };
                        return <UplotReact
                            options={chartOptionsUpdated}
                            data={this.state.streamingStats}
                            dataHash={this.state.streamingStatsDataHash}
                        />;
                    }} />
                </div>
                <GlobalStatsComponent
                    playersMatched={this.state.globalStats.playersMatched}
                    averageMatchFillRate={this.state.globalStats.averageMatchFillRate}
                    averageMatchTimeSeconds={this.state.globalStats.averageMatchTimeSeconds} 
                />
                <table className="table table-sm">
                    <tbody>
                        {rows}
                    </tbody>
                </table>
            </>
        );
    }

}