import React from "react";

interface GlobalStatsProps {
    playersMatched: number;
    averageMatchTimeSeconds: number;
    averageMatchFillRate: number;
}

export class GlobalStats extends React.Component<GlobalStatsProps> {
    constructor(props: GlobalStatsProps) {
        super(props)
    }

    public shouldComponentUpdate(nextProps: GlobalStatsProps) {
        return this.props.playersMatched !== nextProps.playersMatched ||
            this.props.averageMatchTimeSeconds !== nextProps.averageMatchTimeSeconds ||
            this.props.averageMatchFillRate !== nextProps.averageMatchFillRate;
    }

    public render() {
        return (
            <div className="row mb-3 mt-3">
                <div className="col-4">
                    <div className="card w-100">
                        <div className="card-body">
                            <h5 className="mb-2">Players Matched</h5>
                            <h4 className="card-title mb-0"><strong>{this.props.playersMatched}</strong></h4>
                        </div>
                    </div>
                </div>
                <div className="col-4">
                    <div className={"card w-100"}>
                        <div className="card-body">
                            <h5 className="mb-2">Average Time to Match</h5>
                            <h4 className="card-title mb-0"><strong>{Math.round(this.props.averageMatchTimeSeconds)} secs</strong></h4>
                        </div>
                    </div>
                </div>
                <div className="col-4">
                    <div className={"card w-100"}>
                        <div className="card-body">
                            <h5 className="mb-2">Average Fill Rate</h5>
                            <h4 className="card-title mb-0"><strong>{Math.round(this.props.averageMatchFillRate * 100)}%</strong></h4>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}