import { faBriefcase } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

interface JobEntryProps {
    jobId: string;
}

export class JobEntry extends React.Component<JobEntryProps> {
    constructor(props: JobEntryProps) {
        super(props)
    }

    public shouldComponentUpdate(nextProps: JobEntryProps) {
        return this.props.jobId != nextProps.jobId;
    }

    public render() {
        return (
            <tr>
                <td width={24}>&nbsp;</td>
                <td colSpan={4}>
                    <FontAwesomeIcon className="mr-2" icon={faBriefcase} fixedWidth />
                    {"Job #" + this.props.jobId}
                </td>
            </tr>
        );
    }
}