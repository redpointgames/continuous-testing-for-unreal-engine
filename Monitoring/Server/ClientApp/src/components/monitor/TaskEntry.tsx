import { faCheckCircle, faChevronLeft, faChevronRight, faClock, faCog, faExclamationTriangle, faExpand, faQuestion, faRobot, faTimesCircle, faUser, faUsers } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as React from 'react';
import { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { TaskInfo, TaskTree } from '../../data/TaskTree';

function stringToColor(str: string) {
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    let colour = '#';
    for (let i = 0; i < 3; i++) {
        let value = (hash >> (i * 8)) & 0xFF;
        colour += ('00' + value.toString(16)).substr(-2);
    }
    return colour;
}

interface TaskEntryProps {
    task: TaskInfo;
    userId: string | undefined;
    partyId: string | undefined;
    showJobLevel?: boolean;
}

export class TaskEntry extends React.Component<TaskEntryProps> {
    constructor(props: TaskEntryProps) {
        super(props);
    }

    public shouldComponentUpdate(nextProps: TaskEntryProps) {
        return (this.props.task !== nextProps.task);
    }

    public render() {
        let fullTaskId = this.props.task.runId + ":" + this.props.task.jobId + ":" + this.props.task.taskId;
        let icon = <FontAwesomeIcon className="mr-2" icon={faCog} spin fixedWidth />;
        if (this.props.task.isSuccess !== null) {
            if (this.props.task.isSuccess) {
                icon = <FontAwesomeIcon className="mr-2 text-success" icon={faCheckCircle} fixedWidth />;
            } else {
                icon = <FontAwesomeIcon className="mr-2 text-danger" icon={faTimesCircle} fixedWidth />;
            }
        }

        let infos = [];
        if (this.props.userId !== undefined) {
            infos.push(
                <div key="actingUserId" className="mb-1">
                    <FontAwesomeIcon style={{ color: stringToColor(this.props.userId) }} icon={faUser} fixedWidth /> {this.props.userId}
                </div>
            );
        }
        if (this.props.partyId !== undefined) {
            infos.push(
                <div key="actingPartyId" className="mb-1">
                    <FontAwesomeIcon style={{ color: stringToColor(this.props.partyId) }} icon={faUsers} fixedWidth /> {this.props.partyId}
                </div>
            );
        }
        if (this.props.task.durationSeconds !== null) {
            let durationMinutes = Math.floor(this.props.task.durationSeconds / 60);
            let durationSeconds = Math.floor(this.props.task.durationSeconds % 60);
            let minute = durationMinutes === 1 ? "min" : "mins";
            let second = durationSeconds === 1 ? "sec" : "secs";
            if (durationMinutes >= 1) {
                infos.push(
                    <div key="durationSeconds" className="mb-1">
                        <FontAwesomeIcon icon={faClock} fixedWidth /> {durationMinutes} {minute} {durationSeconds} {second}
                    </div>
                );
            } else {
                infos.push(
                    <div key="durationSeconds" className="mb-1">
                        <FontAwesomeIcon icon={faClock} fixedWidth /> {durationSeconds} {second}
                    </div>
                );
            }
        }

        if (this.props.task.currentStatus !== null) {
            infos.push(
                <div key="currentStatus">
                    {this.props.task.currentStatus}<br />
                    <small>{this.props.task.currentDetail}</small>
                </div>
            );
        }
        if (this.props.task.teams !== null) {
            let teamIcons = [];
            let i = 0;
            for (const team of this.props.task.teams) {
                teamIcons.push(<FontAwesomeIcon key={i++} icon={faChevronLeft} fixedWidth />);
                for (const slot of team.slots) {
                    if (slot.type === "User") {
                        teamIcons.push(<FontAwesomeIcon key={i++} style={{ color: stringToColor(slot.userId) }} icon={faUser} fixedWidth />);
                    } else if (slot.type === "AI") {
                        teamIcons.push(<FontAwesomeIcon key={i++} icon={faRobot} fixedWidth />);
                    } else if (slot.type === "Empty") {
                        teamIcons.push(<FontAwesomeIcon key={i++} icon={faExpand} fixedWidth />);
                    } else {
                        teamIcons.push(<FontAwesomeIcon key={i++} icon={faQuestion} fixedWidth />);
                    }
                }
                teamIcons.push(<FontAwesomeIcon key={i++} icon={faChevronRight} fixedWidth />);
            }
            infos.push(
                <div key="teamInfo">
                    {teamIcons}
                </div>
            );
        }
        if (this.props.task.criticalErrorMessage !== null) {
            infos.push(<div key="criticalError" className="text-danger">{this.props.task.criticalErrorMessage}</div>);
            const heartbeaterMatch = (/Expected to get a heartbeat for lobby '(.*)'/).exec(this.props.task.criticalErrorMessage.trim());
            if (heartbeaterMatch !== null) {
                infos.push(<div key="criticalErrorHeartbeater"><Link to={"/logs/heartbeater/" + heartbeaterMatch[1]}>View logs of job responsible for heartbeating</Link></div>);
            }
        }
        if (this.props.task.recentLines !== null && this.props.task.isSuccess === false) {
            infos.push(<small key="recentLines"><pre className="pre-scrollable p-2 mt-2" style={{ whiteSpace: "pre-wrap", maxWidth: "100%", maxHeight: "10em", background: "#fcfcfc", color: "#666", borderRadius: "5px" }}><code>{this.props.task.recentLines}</code></pre></small>);
        }
        let stats = [];
        if (this.props.task.warningCount > 0 || this.props.task.errorCount > 0) {
            stats.push(<br key="newline" />);
            stats.push(
                <small>
                    <span className="ml-5">Logs:</span>
                    {this.props.task.warningCount > 0
                        ? <Fragment key="warnings"><FontAwesomeIcon className="text-warning ml-1" icon={faExclamationTriangle} fixedWidth />&nbsp;{this.props.task.warningCount}</Fragment>
                        : null}
                    {this.props.task.errorCount > 0
                        ? <Fragment key="errors"><FontAwesomeIcon className="text-danger ml-1" icon={faTimesCircle} fixedWidth />&nbsp;{this.props.task.errorCount}</Fragment>
                        : null}
                </small>
            )
        }
        if (this.props.task.isSuccess === null && this.props.task.firstSeenUtc < ((Date.now() / 1000) - 60 * 15)) {
            stats.push(<br key="newline" />);
            stats.push(<span className="badge badge-secondary">Stalled</span>);
        }
        
        return (
            <tr>
                <td width={24}>&nbsp;</td>
                {this.props.showJobLevel ? <td width={24}>&nbsp;</td> : null}
                <td colSpan={this.props.showJobLevel ? 1 : 2} style={{ whiteSpace: "nowrap", width: "20%" }}>
                    {icon}
                    <Link to={"/logs/general/" + fullTaskId}>{"Task " + this.props.task.taskId}</Link>
                    {stats}
                </td>
                <td>
                    {infos}
                </td>
            </tr>
        );
    }
}