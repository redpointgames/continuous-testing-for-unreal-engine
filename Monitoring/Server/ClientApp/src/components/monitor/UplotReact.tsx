import React, {useEffect, useRef} from 'react';

import uPlot from 'uplot';

type OptionsUpdateState = 'keep' | 'update' | 'create';

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/is
if (!Object.is) {
    // eslint-disable-next-line
    Object.defineProperty(Object, "is", {value: (x: any, y: any) =>
        (x === y && (x !== 0 || 1 / x === 1 / y)) || (x !== x && y !== y)
    });
}

export const optionsUpdateState = (_lhs: uPlot.Options, _rhs: uPlot.Options): OptionsUpdateState => {
    const {width: lhsWidth, height: lhsHeight, ...lhs} = _lhs;
    const {width: rhsWidth, height: rhsHeight, ...rhs} = _rhs;

    let state: OptionsUpdateState = 'keep';
    if (lhsHeight !== rhsHeight || lhsWidth !== rhsWidth) {
        state = 'update';
    }
    if (Object.keys(lhs).length !== Object.keys(rhs).length) {
        return 'create';
    }
    for (const k of Object.keys(lhs)) {
        let lhs2: any = lhs;
        let rhs2: any = rhs;
        let k2: any = k;
        if (!Object.is(lhs2[k2], rhs2[k2])) {
            state = 'create';
            break;
        }
    }
    return state;
}

export const dataMatch = (lhs: uPlot.AlignedData, rhs: uPlot.AlignedData): boolean => {
    if (lhs.length !== rhs.length) {
        return false;
    }
    return lhs.every((lhsOneSeries, seriesIdx) => {
        const rhsOneSeries = rhs[seriesIdx];
        if (lhsOneSeries.length !== rhsOneSeries.length) {
            return false;
        }
        return lhsOneSeries.every((value, valueIdx) => value === rhsOneSeries[valueIdx]);
    });
}

export default function UplotReact({options, data, dataHash, target, onDelete = () => {}, onCreate = () => {}}: {
    options: uPlot.Options,
    data: uPlot.AlignedData,
    // eslint-disable-next-line
    target?: HTMLElement | ((self: uPlot, init: Function) => void),
    onDelete?: (chart: uPlot) => void
    onCreate?: (chart: uPlot) => void,
    dataHash: number,
}): JSX.Element | null {
    const chartRef = useRef<uPlot | null>(null);
    const targetRef = useRef<HTMLDivElement>(null);

    function destroy(chart: uPlot | null) {
        if (chart) {
            onDelete(chart);
            chart.destroy();
            chartRef.current = null;
        }
    }
    function create() {
        const newChart = new uPlot(options, data, target || targetRef.current as HTMLDivElement)
        chartRef.current = newChart;
        onCreate(newChart);
    }
    // componentDidMount + componentWillUnmount
    useEffect(() => {
        create();
        return () => {
            destroy(chartRef.current);
        }
    }, []);
    // componentDidUpdate
    const prevProps = useRef({options, data, dataHash, target}).current;
    useEffect(() => {
        const chart = chartRef.current;
        if (prevProps.options !== options) {
            const optionsState = optionsUpdateState(prevProps.options, options);
            if (!chart || optionsState === 'create') {
                console.warn("re-creating uplot graph - this is expensive!");
                destroy(chart);
                create();
            } else if (optionsState === 'update') {
                chart.setSize({width: options.width, height: options.height});
            }
        }
        let didSetData = false;
        if (prevProps.data !== data) {
            if (!chart) {
                create();
            } else if (!dataMatch(prevProps.data, data)) {
                chart.setData(data);
                didSetData = true;
            }
        }
        if (prevProps.dataHash !== dataHash && !didSetData) {
            chart?.setData(data);
        }
        if (prevProps.target !== target) {
            destroy(chart);
            create();
        }

        return () => {
            prevProps.options = options;
            prevProps.data = data;
            prevProps.dataHash = dataHash;
            prevProps.target = target;
        };
    }, [options, data, dataHash, target]);

    return target ? null : <div ref={targetRef}></div>;
}