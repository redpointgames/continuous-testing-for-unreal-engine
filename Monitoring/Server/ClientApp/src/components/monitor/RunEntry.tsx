import { faRunning } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

interface RunEntryProps {
    runId: string;
}

export class RunEntry extends React.Component<RunEntryProps> {
    constructor(props: RunEntryProps) {
        super(props)
    }

    public shouldComponentUpdate(nextProps: RunEntryProps) {
        return this.props.runId != nextProps.runId;
    }

    public render() {
        return (
            <tr>
                <td colSpan={5}>
                    <FontAwesomeIcon className="mr-2" icon={faRunning} fixedWidth />
                    {this.props.runId}
                </td>
            </tr>
        );
    }
}