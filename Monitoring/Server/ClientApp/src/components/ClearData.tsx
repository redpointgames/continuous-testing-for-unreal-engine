import React, { Component } from 'react';
import { Redirect } from 'react-router';

interface ClearDataState {
  status: string;
}

export class ClearData extends Component<{}, ClearDataState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      status: "none"
    };
  }

  private async clearAllData(ev: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    this.setState({status: "running"});
    const response = await fetch("/api/clear", {
      method: "post"
    });
    if (response.ok) {
      this.setState({status: "success"});
    } else {
      this.setState({status: "failed"});
    }
    ev.preventDefault();
  }

  public render () {
    let element = null;
    switch (this.state.status){ 
      case "none": {
        element = <button className="btn btn-danger" onClick={this.clearAllData.bind(this)}>Clear all data</button>;
        break;
      }
      case "running": {
        element = <button className="btn btn-danger disabled">Clear all data</button>;
        break;
      }
      case "failed": {
        element = <button className="btn btn-danger" onClick={this.clearAllData.bind(this)}>Clear all data (failed)</button>;
        break;
      }
      case "success": {
        element = <Redirect to="/" />;
      }
    }
    return (
      <>
        <p>
          Really clear all data?
        </p>
        <p>
          {element}
        </p>
      </>
    );
  }
}
