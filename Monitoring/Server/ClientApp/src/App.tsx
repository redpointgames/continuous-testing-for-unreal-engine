import React, { Component } from 'react';
import { Route, Switch } from 'react-router';
import { Layout } from './components/Layout';
import { Monitor } from './components/Monitor';
import { LogView } from './components/LogView';
import { ClearData } from './components/ClearData';

import './custom.css'

function NotFound() {
  return (
    <p>
      The page you requested was not found.
    </p>
  );
}

export default class App extends Component {
  static displayName = App.name;

  render() {
    return (
      <Layout>
        <Switch>
          <Route exact path='/' component={Monitor} />
          <Route path='/logs/general/:id' render={routeProps => {
            const id = (routeProps.match.params as any).id;
            return (
              <LogView type="fixed" id={id} />
            );
          }} />
          <Route path='/logs/heartbeater/:id' render={routeProps => {
            const id = (routeProps.match.params as any).id;
            return (
              <LogView type="heartbeater" id={id} />
            );
          }} />
          <Route exact path='/clear' component={ClearData} />
          <Route component={NotFound} />
        </Switch>
      </Layout>
    );
  }
}