﻿using Microsoft.AspNetCore.SignalR;
using Server.Database;
using Server.Readers;
using System.Threading.Tasks;

namespace Server
{
    public class MatchmakingDataHub : Hub
    {
        private readonly ITaskReader _reader;

        public MatchmakingDataHub(ITaskReader reader)
        {
            _reader = reader;
        }

        public override async Task OnConnectedAsync()
        {
            var (runs, jobs, tasks) = _reader.GetCurrentStateForInit();
            await Clients.Caller.SendAsync("Init", runs, jobs, tasks, _reader.GetStreamingStats(), _reader.GetGlobalStats());
        }
    }
}
